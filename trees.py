__author__ = "Tim Swarts"


from sklearn import tree
import matplotlib.pyplot as plt


def tree_classifier(inst, attr, data):
    """
    :param inst: Training data labels
    :param attr: Training data attributes
    :param data: Data-to-be-labeled
    :return: Labels for the input data
    """
    clf = tree.DecisionTreeClassifier(criterion="entropy", min_samples_split=5)
    clf = clf.fit(attr, inst)
    plt.figure()
    _ = tree.plot_tree(clf, class_names=["A", "B", "C"], filled=True)
    plt.show()
    return clf.predict(data)
