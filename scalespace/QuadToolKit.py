import sys
from pyquadkey2.quadkey import QuadKey


def createQuadKey(zoomLevel):
    """[createQuadKey]
    Generates all possible quadkey values for a certain zoomlevel
    Args:
        zoomLevel ([int / list[int]]): [zoomlevel(s) for to generate the quadkey names required]
    returns List of Quadkeys for specific zoomlevel(s)
    """

    def _quadkeyEngine(zRange):
        quadKeys = []
        for z in zRange:
            if z == 1:
                quadKeys += [0, 1, 2, 3]
            for q in range(4):
                if z == 1:
                    pass
                else:
                    qk = QuadKey(str(q))
                    quadKeys += qk.children(z)
        return quadKeys

    if type(zoomLevel) == list:
        return _quadkeyEngine(zoomLevel)
    else:
        return _quadkeyEngine([zoomLevel])


if __name__ == "__main__":
    sys.exit()
