def transpose(name, cor, width=100):

    def mult(c):
        return 2-(c/width)
    
    x, y = cor[0], cor[1]
    m = [mult(c) for c in cor]
    mx, my = m[0], m[1]

    name = list(name)

    try:
        quarter = int(name.pop())
    except IndexError:
        return cor

    if quarter == 0:
        x = x / mx
        y = y / my
        return [x,y]

    elif quarter == 1:
        x = x * mx
        y = y / my
        return [x,y]

    elif quarter == 2:
        x = x / mx
        y = y * my
        return [x,y]

    elif quarter == 3:
        x = x * mx
        y = y * my
        return [x,y]

    else:
        raise NameError("No valid quartername has been found...")


print(transpose("023", [0, 20]))