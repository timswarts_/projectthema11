#! /homes/jgray/miniconda3/envs/EM/bin/python3
# imports
import sys
import os
from os.path import join
import argparse as ap
import cv2
import QuadToolKit

class ImageTiler:
    """This module is able to cut large image-files in uniform tiles with the same ratio
    Library requirements for the use of this package:
    - argparse
    - OpenCV

    """
    __author__ = "James Gray"

    def __init__(self, images, zoom):
        self.images = images
        self.zoom = [int(x) for x in zoom[0].split(',')]
        self.splits = [2**int(x) for x in self.zoom]

    def setZoom(self, zoom):
        self.zoom = zoom

    def __repr__(self):
        """Text representation of the class, prints the name of images loaded.
        """
        text = "zoom:" + str(self.zoom) + '\nsplits:'+ str(self.splits) +"\nLoaded images:\n"

        # If images are loaded in the class
        if not len(self.images) == 0:
            for file in self.images:
                text += file + '\n'
            return text
        # If the list is empty
        else:
            return "No images loaded..."

    def load_images(self, path):
        """
        Load image(s) to be tiled
        """
        self.images = os.listdir(path)
        pass

    def tile(self, split, zoom):
        """
        This function cuts the file into uniform pieces and saves the file 
        with a custom name 

        arguments:
        pieces  =  splits you want for horizontal and vertical 
        the pieces will be stored in a directory based on the size of the 
        vertical and horizontal pieces.
        """

        for file in self.images:
            self._tile_engine(file, split, zoom)
        pass

    def _tile_engine(self, file, split, zoom):
        """
        Logic behind tile method, only cuts one file.
        """

        im = cv2.imread(file)
        pieces = split
        imgheight = im.shape[0]
        imgwidth = im.shape[1]

        M = imgheight//pieces
        N = imgwidth//pieces

        names = iter(QuadToolKit.createQuadKey(zoom))

        for y in range(0, imgheight, M):
            for x in range(0, imgwidth, N):
                y1 = y + M
                x1 = x + N
                tiles = im[y:y+M,x:x+N]
                #filename = "image_" + str(x) + '_' + str(y) + ".tiff"
                filename = str(next(names))
                height, width = tiles.shape[:2]
                # if not this directory exists
                if not os.path.isdir(self._path_name(file)):
                    os.mkdir(self._path_name(file))
                    if height / width == 1:
                        cv2.imwrite(os.path.join(self._path_name(file),filename+".tif"), tiles)
                        print("filename", filename, "\twritten to", self._path_name(file))
                else:
                    cv2.imwrite(os.path.join(self._path_name(file),filename+".tif"), tiles)
                    print("filename", filename, "\twritten to", self._path_name(file))
                    
    def _path_name(self, file):
        """
        Create path name dependant on the amount of pieces the image is tiled
        such that the split files are compartementialised in their own folder
        """
        file = file.split(".")[:-1]
        filepath = os.path.join("zoom_"+file[0])
        return filepath


if __name__ == "__main__":
    parser = ap.ArgumentParser(description="""To run this module from the command-line \n
                                            the following parameters must be used:\n
                                            --input\n
                                            --zoom""")

    parser.add_argument("--input", "-i", nargs="+", 
    help="Define the filename(s) of the file(s) to be tiled")
    parser.add_argument("--zoom", "-z", nargs="+",
    help="Define the zoom of tiling or a range of zooms to be tiled (seperated by comma)")
    args = parser.parse_args()
    if not len(sys.argv) == 1:
        tiler = ImageTiler(args.input, args.zoom)
        print(tiler)
        try:
            input("Press Enter to tile the images...\nPress Ctrl+c to exit...")
            pass
        except KeyboardInterrupt:
            print('\n\nprogram exited...')
            sys.exit(0)
        for split, zoom in zip(tiler.splits, tiler.zoom):
            tiler.tile(split, zoom)
    else:
        print('Missing parameters, please refer to the help page (--help / -h)')
