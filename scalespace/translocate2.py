import sys


def transpose2(name, cor, width, height):
    """
    Find corresponding coordinates in higher zoomlevels
    :param name: string - name of the image
    :param cor: list or tuple - coordinates in format (x, y)
    :param width: width of images
    :param height: height of images
    :return:
    """
    # Get coordinates and quarter
    xo, yo, = cor[0], cor[1]
    quarter = int(name.split('.')[0][-1])

    # Calculate new coordinates for found quarter
    if quarter == 0:
        xn = xo/2
        yn = yo/2
        return [round(xn + 0.1), round(yn + 0.1)]

    if quarter == 1:
        xn = width/2 + xo/2
        yn = yo/2
        return [round(xn + 0.1), round(yn + 0.1)]

    if quarter == 2:
        xn = xo/2
        yn = height/2 + yo/2
        return [round(xn), round(yn)]

    if quarter == 3:
        xn = width/2 + xo/2
        yn = height/2 + yo/2
        return [round(xn + 0.1), round(yn + 0.1)]

    print(quarter, "is not a valid quarter")
    sys.exit(1)
    # raise NameError("No valid quartername has been found...")


print(transpose2("002.tif", (300, 940), 1024, 1024))
