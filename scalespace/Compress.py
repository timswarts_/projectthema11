import sys
import ffmpeg

__author__ = "James Gray"


def scale_image(name, width, height=-1):
    """
    method that scales images to a desired resolution

    scale_image(name, width, height=-1)

    name: str, name of image to be loaded from directory
    width: int, scale to amount of pixels
    height: int, scale of height, default is same as width
    """
    (ffmpeg.input(name).
     filter('scale', w=width, h=height).
     output(width + name).
     run())


if __name__ == "__main__":
    args = sys.argv[1:]
    if len(args) < 2:
        print("""
     method that scales images to a desired resolution
 
     scale_image(name, width, height=-1)
 
     name: str, name of image to be loaded from directory
     width: int, scale to amount of pixels
     height: int, scale of height, default is same as width
     """)
    else:
        scale_image(args[0], args[1])
