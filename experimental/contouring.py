import numpy as np
from PIL import Image, ImageOps
import matplotlib.pyplot as plt
from skimage.filters import gaussian
from skimage.segmentation import active_contour

# Load image and convert to grayscale
image = Image.open('/commons/Themas/Thema11/Giepmans/WorkdirJT/image_14560_0.tiff')
image = ImageOps.grayscale(image)
# Convert image to numpy array
data = np.asarray(image)
print(data)

# Set parameters
s = np.linspace(0, 2*np.pi, 400)
print(len(s))
r = 500 + 200*np.sin(s)
c = 900 + 200*np.cos(s)
init = np.array([r, c]).T
# Draw contour
snake = active_contour(gaussian(data, 3),
                       init, alpha=0.14, beta=11, gamma=0.001, w_edge=3.4, max_px_move=3)
# Create result image
fig, ax = plt.subplots(figsize=(7, 7))
ax.imshow(data, cmap=plt.cm.gray)
ax.plot(init[:, 1], init[:, 0], '--r', lw=3)
ax.plot(snake[:, 1], snake[:, 0], '-b', lw=3)
ax.set_xticks([]), ax.set_yticks([])
ax.axis([0, data.shape[1], data.shape[0], 0])
# Display output
plt.show()
