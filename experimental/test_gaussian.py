from scipy.ndimage import gaussian_filter
import numpy as np
from PIL import Image, ImageOps
from math import ceil

image = Image.open("../test.tiff")
image = image.convert('RGB')
image_matrix_rgb = np.asarray(image)
image = ImageOps.grayscale(image)
image_matrix_gray = np.asarray(image)
image = gaussian_filter(image_matrix_gray, sigma=2)
image = Image.fromarray(image)
image.show()
