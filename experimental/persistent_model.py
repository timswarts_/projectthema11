import pickle
from os import listdir
from os.path import join

import cv2
import numpy as np
from joblib import dump, load
from numpy.core.fromnumeric import reshape
from sklearn.neural_network import MLPClassifier

"""
Experimental module that saves a persistent model for image classification.
"""

directory_path = "/Users/jamesgray/Documents/code/Project-Thema-11/classification-images/nucleus"

model_path = "/Users/jamesgray/Documents/code/Project-Thema-11/experimental"

files_in_dir = listdir(directory_path)

class_label = 1
image_list = []
size = 256, 256

# read files
for i, file in enumerate(files_in_dir, start=1):
    if not file.startswith("."):
        file_path = join(directory_path, file)
        file = cv2.imread(file_path)
        resized_image = cv2.resize(file, dsize=size, interpolation=cv2.INTER_CUBIC)
        gray_scale_image = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)
        image_list.append(gray_scale_image)

# train the model
classifyer = MLPClassifier(hidden_layer_sizes=150,
                           activation='relu',
                           alpha=0.0001)

print('fitting images')
for i, image in enumerate(image_list, start=1):
    print(f'fittin image: {i}')
    classifyer.fit(image, np.full(size, class_label))


print('dumping model')
model = pickle.dumps(classifyer)
dump(model, join(model_path, "Classifyer.pkl"))

print('Classification done!')
