import numpy as np
from PIL import Image, ImageOps
import matplotlib.pyplot as plt
from skimage.feature import match_template
from skimage.feature import peak_local_max

# Load image and convert to grayscale numpy array
image = Image.open('/commons/Themas/Thema11/Giepmans/WorkdirJT/image_14560_0.tiff')
image = ImageOps.grayscale(image)
data = np.asarray(image)
# Load template image and convert to grayscale numpy array
template = Image.open('/homes/cvanbuiten/Desktop/HighContrastTest.png')
template = ImageOps.grayscale(template)
template = np.asarray(template)

# Perform the template matching
result = match_template(data, template, mode='edge')

ij = np.unravel_index(np.argmax(result), result.shape)
x, y = ij[::-1]

# Create the output figure
fig = plt.figure(figsize=(8, 3))
ax1 = plt.subplot(1, 3, 1)
ax2 = plt.subplot(1, 3, 2)
ax3 = plt.subplot(1, 3, 3, sharex=ax2, sharey=ax2)

# Display template image
ax1.imshow(template, cmap=plt.cm.gray)
ax1.set_axis_off()
ax1.set_title('template')

# Display target image
ax2.imshow(image, cmap=plt.cm.gray)
ax2.set_axis_off()
ax2.set_title('image')
# Draw matches
template_width, template_height = template.shape
rect = plt.Rectangle((x, y), template_width, template_height, edgecolor='b', facecolor='none')
ax2.add_patch(rect)
for x, y in peak_local_max(result, threshold_abs=0.15, exclude_border=10):
    rect = plt.Rectangle((y, x), template_height, template_width, edgecolor='r', fc='none')
    ax2.add_patch(rect)

# Display matching results
ax3.imshow(result)
ax3.set_axis_off()
ax3.set_title('`match_template`\nresult')
# Draw best match
ax3.autoscale(False)
ax3.plot(x, y, 'o', markeredgecolor='r', markerfacecolor='none', markersize=10)

# Display output
plt.show()
