__author__ = "Tim Swarts"

import numpy as np
from PIL import Image, ImageOps
from classes.pixel_data import create_neighbour_matrix
from scalespace.translocate2 import transpose2


class ImageHandler:
    def __init__(self, image_name, pixel_neighbour_range, y_steps=2):
        """
        Class for loading in an image and it's pixels
        :param image_name: filename or path from where the image is loaded
        :param pixel_neighbour_range: The range in which to search for neighbours.
                                      Starting at the center pixel the range reaches into each direction
                                      (a range of 3 will give a 7x7 matrix, 1 (center pixel) + 2 * range)
        """
        # Save name
        self.image_name = image_name
        # Save range
        self.neigh_range = pixel_neighbour_range
        # Save scalespace boolean
        # Load in and greyscale the image, then turn it into a numpy array
        image = Image.open(self.image_name)
        image = image.convert('RGB')
        self.image_matrix_rgb = np.asarray(image)
        image = ImageOps.grayscale(image)
        self.image_matrix_gray = np.asarray(image)
        # Get dimensions of grey scale matrix
        self.height = len(self.image_matrix_gray)
        self.width = int(self.image_matrix_gray.size / self.height)
        # Number of rows to skip +1
        self.y_steps = y_steps
        # Load in list of pixel attributes
        self.pixel_attributes = self.get_pixel_list()

    def get_pixel_attributes(self):
        """
        Return a list of neighbour matrices(in list form)
        :return pixel_attributes: list of lists of neighbours for each pixel
        """
        return self.pixel_attributes

    def create_neighbour_matrix(self, m_range, coordinates):
        """
        This function creates a list of neighbouring pixels given a pixel's coordinates and a range around the pixel
        :param m_range: The range in which to create the matrix. Starting at the center pixel the range reaches into
                        each direction (a range of 3 will give a 7x7 matrix, 1 (center pixel) + 2 * m_range)
        :param coordinates: A tupel containing the coordinates of the center pixel around which neighbours will be
                            searched. Order: x, y
        :return neighbour_list: The matrix of neighbouring pixels
        """

        # Extract coordinates
        cx, cy = coordinates[0], coordinates[1]
        x_coords = [cx - m_range + i for i in range(m_range * 2 + 1)]
        y_coords = [cy - m_range + i for i in range(m_range * 2 + 1)]

        # Create empty list
        neighbour_list = []

        # Loop through coords
        for cy in y_coords:
            for cx in x_coords:
                # Check of coords are within image bounds
                if cx < 0 or cy < 0 or cx > self.width - 1 or cy > self.height - 1:
                    # if not, add -1
                    neighbour_list.append(-1)
                else:
                    # else, add value
                    neighbour_list.append(self.image_matrix_gray[cy][cx])

        return neighbour_list

    def get_pixel_list(self):
        """
        Do things
        :return:
        """
        pixel_list = []
        for y in range(0, self.height, self.y_steps):
            for x in range(self.width):
                pixel_list.append(create_neighbour_matrix(self.neigh_range, (x, y), self.image_matrix_gray))
        return pixel_list

    def get_rgb(self):
        """
        Return the rgb matrix of the image
        :return image_matrix_rgb: numpy array containing the rgb values of each pixel in the image
        """
        return self.image_matrix_rgb

    def __repr__(self):
        """
        Representative for printing instance
        :return: String to print
        """
        return f"Image name: {self.image_name}"


class ScaleSpaceHandler(ImageHandler):
    def __init__(self, parent_name, image_name, pixel_neighbour_range, y_steps=2):
        # Call ImageHandler init
        super().__init__(image_name, pixel_neighbour_range, y_steps)
        # Set parten name
        self.parent_name = parent_name
        # Rewrite pixel attributes to new variant
        self.pixel_attributes = self.get_scale_space_pixel_list()
        # Load in and greyscale the image, then turn it into a numpy array
        parent = Image.open(self.parent_name)
        parent = parent.convert('RGB')
        self.parent_matrix_rgb = np.asarray(parent)
        parent = ImageOps.grayscale(parent)
        self.parent_matrix_gray = np.asarray(parent)

    def get_scale_space_pixel_list(self):
        """
        Do things
        :return:
        """
        pixel_list = []
        for y in range(0, self.height, self.y_steps):
            for x in range(self.width):
                pixel_list.append(create_neighbour_matrix(self.neigh_range, (x, y), self.image_matrix_gray) +
                                  create_neighbour_matrix(self.neigh_range, transpose2(self.image_name, (x, y),
                                                                                       self.width, self.height),
                                                          self.parent_matrix_gray))
        return pixel_list
