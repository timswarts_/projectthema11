from sklearn import tree
import matplotlib as plt

X = [[0, 1, 0], [1, 0, 1], [0, 0, 0]]
Y = ["A", "B", "C"]
clf = tree.DecisionTreeClassifier()
clf = clf.fit(X, Y)
print(clf.predict([[0, 0, 0], [0, 1, 0], [1, 0, 1]]))
print(clf.predict([[1, 1, 1]]))
print(clf.get_params())
