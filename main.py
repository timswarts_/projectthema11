import sys
import numpy as np
from PIL import Image, ImageOps
import matplotlib.pyplot as plt
from random import randint
from classes.marker2 import Marker
from classes.model_input_handler import ImageHandler
from neuralnetwork import sk_nn
from naivebayes import naivebayes
from trees import tree_classifier
from classes.label_loader import LabelLoader


def get_coordinates_from_labels(labels, y_steps, img_width, check_label):
    """
    Create a list of coordinates from the labels made by the classifier
    :param labels: list - List of labels made by a classifier
    :param y_steps: int - Increase in y
    :param img_width: int - Width of image in pixels
    :param check_label: string - Label of which to select coordinates
    :return coords: list - Coordinates
    """
    # Initialize empty values
    coords = []
    x = 0
    y = 0

    # Loop through labels
    for label in labels:
        if x > img_width - 1:
            # When x is higher than the image width, reset to 0 and go to next row
            x = 0
            y += y_steps
        if label == check_label:
            # If the label is the one we want to save, append coordinates to coords list
            coords.append([x, y])
        # Increase x
        x += 1
    # print(coords)
    # Return coords list
    return coords


def main():

    # Get new ImageHandler
    print("Loading in image, this might take a while...")
    image = ImageHandler("target_acquired.tif", 3)

    # Get training attributes
    label_image = LabelLoader("part3.tif", 3)
    training_labels = label_image.get_labels()
    training_attributes = label_image.get_attributes("part1.tif")

    # Get testing attributes
    testing_attributes = image.get_pixel_attributes()
    # Testing labels
    labels = naivebayes(training_labels, training_attributes, testing_attributes)

    testmarker = Marker(image.get_rgb())

    width = int(image.image_matrix_gray.size / len(image.image_matrix_gray))
    cols = [(230, 25, 75), (60, 180, 75), (255, 225, 25), (0, 130, 200), (245, 130, 48), (145, 30, 180), (70, 240, 240),
            (240, 50, 230), (210, 245, 60), (250, 190, 212), (0, 128, 128), (220, 190, 255), (170, 110, 40),
            (255, 250, 200), (128, 0, 0), (170, 255, 195), (128, 128, 0), (255, 215, 180), (0, 0, 128), (128, 128, 128),
            (255, 255, 255), (0, 0, 0)]
    i = 0
    for label in set(training_labels):
        label_coords = get_coordinates_from_labels(labels, 2, width, label)
        testmarker.mark(label_coords, cols[i])  # (randint(0, 255), randint(0, 255), randint(0, 255)))
        i += 1

    testmarker.show_results()
    return 0


if __name__ == "__main__":
    sys.exit(main())
