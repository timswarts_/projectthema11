import matplotlib.pyplot as plt
from PIL import Image, ImageOps
import numpy as np


class Marker:

    def __init__(self, img):
        self.img = img.copy()

    def mark(self, coords, col=(255, 0, 0)):
        try:
            for i in coords:
                self.img[i[1], i[0]] = col
        except IndexError:
            pass

    def show_results(self):
        result = Image.fromarray(self.img, 'RGB')
        result.show()

    def save_results(self, path):
        result = Image.fromarray(self.img, 'RGB')
        result.save(path)
