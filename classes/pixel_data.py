from math import sqrt


def create_neighbour_matrix(m_range, coordinates, img):
    """
    This function creates a matrix of neighbouring pixels given a pixel's coordinates and a range around the pixel
    :param m_range: The range in which to create the matrix. Starting at the center pixel the range reaches into each
                    direction (a range of 3 will give a 7x7 matrix, 1 (center pixel) + 2 * m_range)
    :param coordinates: A tupel containing the coordinates of the center pixel around which neighbours will be searched.
                        Order: x, y
    :param img: The image to which the pixel belongs
    :return matrix: The matrix of neighbouring pixels
    """

    # Extract coordinates
    cx, cy = coordinates[0], coordinates[1]
    x_coords = [cx - m_range + i for i in range(m_range * 2 + 1)]
    y_coords = [cy - m_range + i for i in range(m_range * 2 + 1)]

    # Create empty list
    neighbour_list = []

    # Get height and width
    height = len(img)
    width = int(img.size / height)

    # Loop through coords
    for cy in y_coords:
        for cx in x_coords:
            # Check of coords are within image bounds
            if cx < 0 or cy < 0 or cx > width - 1 or cy > height - 1:
                # if not, add -1
                neighbour_list.append(-1)
            else:
                # else, add value
                neighbour_list.append(img[cy][cx])

    return neighbour_list

