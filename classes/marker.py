import matplotlib.pyplot as plt
from PIL import Image, ImageOps
import numpy as np


class Marker:

    def __init__(self, img):
        self.img = img
        self.fig, self.ax = plt.subplots(figsize=(7, 7))

        # Display image
        self.ax.imshow(img, cmap=plt.cm.gray)
        self.ax.set_axis_off()
        self.ax.set_title('Result')

    def mark(self, coords, col=(0.7, 0.2, 0.7, 0.5)):
        for i in coords:
            circ = plt.Circle((i[0], i[1]), 1, edgecolor=(0, 0, 0, 0), facecolor=col, fill=True)
            self.ax.add_patch(circ)


# image = Image.open('../testgrey.png')
# image = ImageOps.grayscale(image)
# data = np.asarray(image)
#
# testmarker = Marker(data)
# testmarker.mark([[1, 1], [0, 1], [5, 5], [0, 0]])
