from classes.model_input_handler import ImageHandler
import numpy as np
from PIL import Image, ImageOps


class LabelLoader:
    def __init__(self, label_image, m_range):
        """
        Class designed to gather all information from a labeled image.
        :param label_image: Path to label image.
        """
        self.alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        self.labels = []
        self.coordinates = []
        self.originals = []
        self.attributes = []
        self.m_range = m_range
        self.label_image = ImageHandler(label_image, self.m_range, 1)
        self.rgb = self.label_image.get_rgb().tolist()

    def get_labels(self):
        """
        Method to gather class labels from image.
        :return: A list of all class labels from the image in left -> right, top -> bottom order.
        """
        if self.labels:
            return self.labels
        else:
            for x in self.rgb:
                for i in x:
                    if i != [0, 0, 0]:
                        if i not in self.originals:
                            self.originals.append(i)
                            self.labels.append(self.alphabet[len(self.originals)])
                        else:
                            self.labels.append(self.alphabet[self.originals.index(i)])
            return self.labels

    def get_coordinates(self):
        """
        Method to gather all coordinates from image.
        Can be run without running get_labels() first.
        :return: A list of coordinate-lists.
        """

        if self.coordinates:
            return self.coordinates
        else:
            # Set starting coordinates
            x, y = 0, 0

            # Loop through the labels
            for _ in self.get_labels():
                # Reset counter and move to next row.
                if x > self.label_image.width - 1:
                    x = 0
                    y += 1
                # Store the values that where selected.
                self.coordinates.append([x, y])
                # Move to next column
                x += 1
            return self.coordinates

    def get_attributes(self, attribute_image):
        """
        Method to gather all neighbour matrices from image.
        Can be run without running get_coordinates() first.
        :param attribute_image: Path to attribute image.
        :return: A list of lists of attributes/neighbour matrices.
        """
        image = ImageHandler(attribute_image, self.m_range)

        if self.attributes:
            return self.attributes
        else:
            for coord in self.get_coordinates():
                self.attributes.append(image.create_neighbour_matrix(self.m_range, coord))
            return self.attributes


# test = LabelLoader("/homes/cvanbuiten/Desktop/part3.tif", 3)
# #
# labels = test.get_labels()
# attributes = test.get_attributes("/homes/cvanbuiten/Desktop/part1.tif")


