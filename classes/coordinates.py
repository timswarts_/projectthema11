import cv2


class Coordinator:
    def __init__(self, img):
        self.img = img
        self.xy = ""
        self.coords = []
        self.col = []

    def on_EVENT_LBUTTONDOWN(self, event, x, y, flags, param):
        """
        Stores coordinates for each mouse click.
        :param event: Left click
        :param x: X Coordinate
        :param y: Y Coordinate
        """
        if event == cv2.EVENT_LBUTTONDOWN:
            self.xy = "%d,%d" % (x, y)
            self.coords.append([x, y])
            cv2.circle(self.img, (x, y), 1, self.col, thickness=-1)
            cv2.imshow("image", self.img)

    def map(self, col=(0, 0, 255)):
        """
        Present image and allow user input selection.
        :return: A list of coordinates.
        """
        self.col = col
        cv2.namedWindow("image")
        cv2.setMouseCallback("image", self.on_EVENT_LBUTTONDOWN)
        cv2.imshow("image", self.img)
        cv2.waitKey(0)
        return self.coords

    def clear(self):
        """
        Clear coordinates
        """
        self.coords = []

