import cv2
import random
import sys
import matplotlib.pyplot as plt
from itertools import count

from classes.coordinates import Coordinator
from classes.marker2 import Marker
from classes.pixel_data import create_neighbour_matrix
from classes.model_input_handler import ImageHandler, ScaleSpaceHandler
from scalespace.translocate2 import transpose2
from naivebayes import naivebayes
from multinominalbayes import multinomial
from neuralnetwork import sk_nn


class Main:
    """
    Main class that runs the image classification program
    """

    # Image objects
    LOADED_IMAGE = None
    COORDINATOR = None

    rgb = [[255, 0, 0], [0, 255, 0], [0,0,255],
            [255, 51, 0],
            [255, 102, 0],
            [255, 153, 0], [255, 204, 0], [255, 255, 0],
                [153, 255, 0], [153, 255, 0], [102, 255, 0],
                [51, 255, 0], ]

    def __init__(self, filename, neighbours, scale_space_parent="", y_steps=2):
        """
        Initialisation of the parameters.
        :param filename: string - name of the file to classify
        :param neighbours: int - range in which to select pixel neighbours for training and classification
        :param scale_space_parent: string - name of the parent file when using scale space,
                                            leave empty for no scale space
        :param y_steps: int - size of steps in y axis when loading in neighbour matrices, y_steps=2 skips one row of
                              pixels each time, loading in only half the image worth of neighbour  matrices. Increase
                              this number for faster loading times, decrease (to a minimum of 1) for a more complete
                              classification.
        """
        self.filename = filename
        self.neighbours = neighbours
        self.scale_space_parent = scale_space_parent
        self.y_steps = y_steps

    def __call__(self):
        """
        run main if class is called as a function.
        """
        self.run()

    def run(self):
        """
        This method is here to make the program run.
        """
        # Load Image handler with specified image
        self.get_image_handler()
        # Load coordinator in memory with specified image
        self.get_coordinator()
        # Get training coordinates
        training_labels, training_coordinates = self.get_training_coordinates()
        # Get training attributes
        expanded_training_coordinates = self.add_neighbours_to_attributes(
            training_coordinates)
        # Get testing attributes
        testing_attributes = self.LOADED_IMAGE.get_pixel_attributes()
        # Test (classify) testing attributes with classifier
        labels = naivebayes(training_labels,
                            expanded_training_coordinates,
                            testing_attributes)
        # Mark the image
        testmarker = Marker(self.LOADED_IMAGE.image_matrix_rgb)
        # Get image width
        width = self.LOADED_IMAGE.width

        for x in set(training_labels):
            testmarker.mark(self.coordinates_from_label_getter(labels, self.y_steps, width, x), self.random_color())
        iter_labels = list(set(training_labels))
        iter_labels.sort()
        training_labels = iter_labels
        for i, x in enumerate(training_labels):
            testmarker.mark(self.coordinates_from_label_getter(
                labels, 2, width, x), self.rgb[i])
        testmarker.show_results()
        return 0

    @staticmethod
    def random_color():
        """
        Generate and return a random color
        :return rgbl: A list containing rgb values
        """
        rgbl = [255, 0, 0]
        random.shuffle(rgbl)
        return rgbl

    def coordinates_from_label_getter(self, labels,
                                      y_steps, image_width,
                                      check_label):
        """
        Create a list of coordinates from the labels made by the 
        classifier
        :param labels: list - List of labels made by a classifier
        :param y_steps: int - Increase in y
        :param image_width: int - Width of image in pixels
        :param check_label: string - Label of which to select 
        coordinates
        :return coords: list - Coordinates
        """
        # Initialize empty values
        selected_coordinates = []
        x, y = 0, 0

        # Loop through the label
        for label in labels:
            # Reset counter and move to next row.
            if x > image_width - 1:
                x = 0
                y += y_steps
            # Store the values that where selected.
            if label == check_label:
                selected_coordinates.append([x, y])
            # Move to next column
            x += 1
        return selected_coordinates

    def automatic_colour_selector(self):
        """
        This function automatically picks a colour
        to assign to a class in the image
        """
        pass

    def get_image_handler(self):
        """
        Load image into the ImageHandler
        """
        print("loading image...")
        if self.scale_space_parent:
            # check for scale space
            image = ScaleSpaceHandler(self.filename, self.neighbours, self.scale_space_parent, self.y_steps)
        else:
            image = ImageHandler(self.filename,
                                 self.neighbours, self.y_steps)
        self.LOADED_IMAGE = image

    def get_coordinator(self):
        """
        Load the image into the class that creates
        the interface.
        """
        file_in = cv2.imread(self.filename)
        coordinator = Coordinator(file_in)
        self.COORDINATOR = coordinator
        print("done!")

    def get_training_coordinates(self):
        """
        Get the training coordinates from the
        user.
        """
        training_labels = []
        all_training_coordinates = []
        next_set_coordinates = [0]
        i = None
        x = count(0)
        colour = 0

        while len(next_set_coordinates):
            i = next(x)
            # Get user-specified coordinates.
            next_set_coordinates = self.COORDINATOR.map(self.rgb[colour])
            all_training_coordinates += next_set_coordinates

            # Assign right class to eacht coordinate.
            for _ in range(len(next_set_coordinates)):
                training_labels.append(chr(65+i))

            print(f"Label {chr(65+i)}: ", next_set_coordinates)
            # Prepare for next iteration
            self.COORDINATOR.clear()
            colour += 1

        return training_labels, all_training_coordinates

    def add_neighbours_to_attributes(self, all_training_coordinates):
        """
        returns a list of lists containing neighbours for each input coordinates.
        """
        if self.scale_space_parent:
            training_attributes = [
                create_neighbour_matrix(
                    self.LOADED_IMAGE.neigh_range, pixel,
                    self.LOADED_IMAGE.image_matrix_gray) +
                create_neighbour_matrix(
                    self.LOADED_IMAGE.neigh_range, transpose2(
                        self.LOADED_IMAGE.image_name, pixel,
                        self.LOADED_IMAGE.width, self.LOADED_IMAGE.height),
                    self.LOADED_IMAGE.parent_matrix_gray)
                for pixel in all_training_coordinates]
        else:
            training_attributes = [create_neighbour_matrix(
                self.LOADED_IMAGE.neigh_range, pixel,
                self.LOADED_IMAGE.image_matrix_gray) for pixel in
                all_training_coordinates]
        return training_attributes


if __name__ == "__main__":
    x = Main(filename='tiles/0332.tif', neighbours=5, scale_space_parent='tiles/1024_033.tif', y_steps=2)
    x()
