from sklearn.neural_network import MLPClassifier

def sk_nn(inst, attr, data):
    """
    :param inst: Training data labels
    :param attr: Training data attributes
    :param data: Data-to-be-labeled
    :return: Labels for the input data
    """
    nn = MLPClassifier(alpha=0.00001)
    return nn.fit(attr, inst).predict(data)