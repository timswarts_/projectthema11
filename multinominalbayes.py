from sklearn.naive_bayes import MultinomialNB

def multinomial(inst, attr, data):
    """
    :param inst: Training data labels
    :param attr: Training data attributes
    :param data: Data-to-be-labeled
    :return: Labels for the input data
    """
    mnb = MultinomialNB(alpha=0.0001)
    return mnb.fit(attr, inst).predict(data)
