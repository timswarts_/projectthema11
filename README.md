# Transmitting Electron Microscopy Organelle Recognition
![alt text](readme/docs/images/logo11.png)


- [About](#about)
  - [Built With](#built-with)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Usage](#usage)
- [Support](#support)
- [License](#license)
- [Acknowledgements](#acknowledgements)

---

## About

> This is commandline tool that aims to classify grayscale images that come from 
> an *electron microscope* (**EM**).
> It uses machine learning to classify organelles and structures in large gray-
> scale images.
> This project aims to give a better understanding in how machine learning can 
> help the modern biologist to classify and understand EM pictures



> Classification Demo
![alt text](readme/docs/images/demo.png)  
![alt text](readme/docs/images/demo2.png)  



### Built With

> * Fiji. (n.d.). ImageJ. https://imagej.net/software/fiji/
> * Matplotlib 3.4.2. (2021, May 8). Matplotlib. https://matplotlib.org/
> * Numpy. (2021, June 22). https://numpy.org/
> * OpenCV – 4.5.2. (2021, April 2). OpenCV. https://opencv.org/releases/
> * Pillow 8.0. (2021). pillow. https://pillow.readthedocs.io/en/stable/installation.html
> * Python 3.8.*. (2021, May 3). Python. https://www.python.org/downloads/
> * Scikit-learn 0.24.2. (2021). Scikit-learn. https://scikit-learn.org/stable/install.html
> * TrakEM2 1.3.6. (n.d.). https://imagej.net/plugins/trakem2/


## Getting Started

### Prerequisites

> * Matplotlib 3+ Matplotlib. https://matplotlib.org/
> * Numpy. https://numpy.org/
> * OpenCV – 4+ OpenCV. https://opencv.org/releases/
> * Pillow 8.0+ pillow. https://pillow.readthedocs.io/en/stable/installation.html
> * Python 3.7+ *(previous versions not tested)* Python. https://www.python.org/downloads/
> * Scikit-learn 0.24.2. Scikit-learn. https://scikit-learn.org/stable/install.html

### Installation

> 1.  Make sure you have all the prerequisites installed  
> 2.  Clone this repository in an emty directory on your hard-drive  
`git clone https://bitbucket.org/timswarts_/projectthema11/src/master/`  


## Usage

### Case 1  
> You want to train and classify a single image without the use of scalespace:  
> * Open `main_refactored.py` in your text editor of choice and scroll down to the last line:  
remove the positional argument `, scale_space_parent='tiles/000.tif'`
```
if __name__ == "__main__":
    x = Main(filename='[YOURIMAGEFILE.EXT]', neighbours=3)
    x()
```  
> * replace `[YOURIMAGEFILE.EXT]` with the filename of of the image you want to use.  
> * set `neighbours` to an integer value. The default is 3, this will be enough in most situations.  
> beware that changing the amount of neighbours can dramatically impact the preformance of the 
script.  
> * Open up a terminal and execute the script:  
`$ ~/Doc/projectthema11: python main_refactored.py`  
> * Dependig on the picture you load the script, you will be greeted with a screen not much 
unlike this one:  
>![alt text](readme/docs/images/step1.png)  
> * Now using the mouse you can select different parts of the image which belong to the same class 
and press ENTER between each class you want to lable and ending with DOUBLE ENTER when you are done 
selecting the classes.  
> * After pressing DOUBLE ENTER you will be returned the final classified picture.  
>![alt text](readme/docs/images/step2.png)  

### Case 2  
> You DO want to use scalespace while training a slingle image.  
> * Return to the first step of [case 1](### Case 1) and make sure the last line 
of `main_refactored.py` is similar to the line pythoncode underneath. Also make sure 
your parent image is complient to the [Bing Maps Tile System](https://docs.microsoft.com/en-us/bingmaps/articles/bing-maps-tile-system). Making 
it so, wil ensure that the classification will take up more information from the parents 
surroundings.
```
if __name__ == "__main__":
    x = Main(filename='tiles/0003.tif', neighbours=3, scale_space_parent='tiles/000.tif')
    x()
```  
> * Afterwards save the script, and run it in the same fashion as [Case 1](### Case 1)  
> * Resuls should be more defined and better separted than the results from Case 1.  
>![alt text](readme/docs/images/step3.png)  

  
### Case 3  
> Training a model with pretrained data is also a possibility. To do so you have 
to make sure you have three images:  
1. An image that will be used as trainingdata. In this example we will be using the following 
image.  
> ![alt text](readme/docs/images/part1.png)  
2. An image that marks the region with a specific grayscale per class. For this example the following 
traing-data will be used.  
> ![alt text](readme/docs/images/part2.png)  
3. An image that will be classified.   


> Open `main.py` in your favorite editor  
> * type the file path of point 1 at `line 76`:  
`    training_attributes = label_image.get_attributes("part1.tif")`  
> * type the filepath of point 2 at `line 74`  
`    label_image = LabelLoader("part2.tif", 3)`  
> * type the image that you want to classify of point 3 at `line 54`  
`    image = ImageHandler("part3.tiff", 3)`  
> * Save the script and run it  
> `$ ~/Doc/projectthema11: python main.py` 
> ![alt text](readme/docs/images/demo.png)  

  
**[!]** Make sure you enter the filename relative to the `main_refactored.py` and `main.py` script.


## Support

> If you have questions or suggestions, you can send us a mail at the following 
email addresses:  
>  c.vanbuiten@st.hanze.nl  
>  j.gray@st.hanze.nl  
>  t.swarts@st.hanze.nl  
  

We would like to hear from you!

## Authors & contributors

The original setup of this repository is by [T. Swarts](https://bitbucket.org/timswarts_), 
[J. Gray](https://bitbucket.org/NeeVork/) and [C. van Buiten](https://bitbucket.org/cvbuiten).

## License

This project is licensed under the **MIT license**.

## Acknowledgements

> This project was created under the wonderfull supervision of D. Langers. At 
Hanze Hogeschool
