from sklearn.naive_bayes import GaussianNB


def naivebayes(inst, attr, data):
    """
    :param inst: Training data labels
    :param attr: Training data attributes
    :param data: Data-to-be-labeled
    :return: Labels for the input data
    """
    gnb = GaussianNB()
    return gnb.fit(attr, inst).predict(data)
